import datetime
import numpy as np

def ler_timestamp(s):
	s = '0'+s if s[1] == '-' else s
	return datetime.datetime.strptime(s, "%d-%b-%Y_%H:%M")

def ler_incerteza(s):
	if s == '—':
		return None
	else:
		l = s.split('±')
		return (float(l[0]), float(l[1]))

def ler_num_ast(s):
	tem_asterisco = s[-1] == '*'
	if not tem_asterisco:
		numero = int(s)
	else:
		numero = int(s[0:-1])
	return (numero, tem_asterisco)

def ler_pos(s):
	l = s.split(',')
	return (int(l[0]), int(l[1]))

def ler_trigger_erupts(s):
	if s == ' ':
		return None
	else:
		return str(s)




data = np.genfromtxt(
	'dados_luna2.txt', 
	dtype='O,O,O,O,O,i,i,O,O',
	encoding='utf-8', 
	missing_values='X', 
	converters={
		0: ler_num_ast, 
		1: ler_timestamp, 
		2: str,
		3: ler_pos, 
		4: str, 
		5: int, 
		6: int, 
		7: str, 
		8: str, 
	})

print(data[0])
