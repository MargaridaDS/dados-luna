import datetime
import numpy as np

def ler_timestamp(s):
	s = '0'+s if s[1] == '-' else s
	return datetime.datetime.strptime(s, "%d-%b-%Y_%H:%M")

def ler_incerteza(s):
	if s == '—':
		return None
	else:
		l = s.split('±')
		return (float(l[0]), float(l[1]))

def ler_num_ast(s):
	tem_asterisco = s[-1] == '*'
	if not tem_asterisco:
		numero = int(s)
	else:
		numero = int(s[0:-1])
	return (numero, tem_asterisco)

data = np.loadtxt(
	'dados_luna.txt', 
	dtype='O,O,i,O,O,O,O,O',
	encoding='utf-8', 
	converters={
		0: ler_num_ast, 
		1: ler_timestamp, 
		2: int,
		3: ler_incerteza, 
		4: ler_incerteza, 
		5: ler_incerteza, 
		6: ler_incerteza, 
		7: ler_incerteza, 
	})

print(data[0])
